//
//  ContactsModel.swift
//  contacts
//
//  Created by Руслан Луценко on 27.10.2021.
//

import UIKit

struct Contact {
    var photo: UIImage?
    var firstName: String?
    var secondName: String?
    var phoneNumber: String?
    var email: String?
    var birthday: String?
    var height: String?
    var drivingLicense: Bool?
    var drivingLicenseDescription: String?
    var notes: String?
    var description: String {
        if (firstName != nil && secondName != nil) && (!firstName!.isEmpty && !secondName!.isEmpty) {
            return "\(firstName!) \(secondName!)"
        } else if firstName != nil && !firstName!.isEmpty {
            return firstName!
        } else if secondName != nil && !secondName!.isEmpty {
            return secondName!
        } else if email != nil && !email!.isEmpty {
            return email!
        }
        return ""
    }
    
    static var contacts = [Contact]()
}

