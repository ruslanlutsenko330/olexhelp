//
//  ContactListViewController.swift
//  contacts
//
//  Created by Руслан Луценко on 28.10.2021.
//

import UIKit

class ContactListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Contact.contacts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactTableViewCell
        
        cell.imageView?.image = Contact.contacts[indexPath.row].photo
        cell.contactDescription.text = Contact.contacts[indexPath.row].description
        cell.contactPhoneNumber.text = Contact.contacts[indexPath.row].phoneNumber
        
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print (Contact.contacts)
        tableView.dataSource = self
        tableView.delegate = self
    }
}
