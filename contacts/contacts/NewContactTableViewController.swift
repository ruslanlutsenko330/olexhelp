//
//  NewContactTableViewController.swift
//  contacts
//
//  Created by Руслан Луценко on 27.10.2021.
//

import UIKit

class NewContactTableViewController: UITableViewController, UIPickerViewDataSource, UIPickerViewDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var contactsPhoto: UIImageView!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var secondNameField: UITextField!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var birthdayField: UITextField!
    @IBOutlet weak var heightField: UITextField!
    @IBOutlet weak var drivingLicenseField: UITextField!
    @IBOutlet weak var drivingLicenseSwitch: UISwitch!
    @IBOutlet weak var notesField: UITextField!
    
    @IBAction func getBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func addPhoto(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let takePhoto = UIAlertAction(title: "Take photo", style: .default) { _ in
            self.choseImagePicker(source: .camera)
        }
        let chosePhoto = UIAlertAction(title: "Chose photo", style: .default) { _ in
            self.choseImagePicker(source: .photoLibrary)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            actionSheet.dismiss(animated: true)
        }
        actionSheet.addAction(takePhoto)
        actionSheet.addAction(chosePhoto)
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true)
    }
    
    @IBAction func saveContact(_ sender: UIBarButtonItem) {
        newContact.firstName = firstNameField.text
        newContact.secondName = secondNameField.text
        newContact.phoneNumber = phoneNumberField.text
        newContact.email = emailField.text
        newContact.birthday = birthdayField.text
        newContact.height = heightField.text
        newContact.drivingLicense = drivingLicenseSwitch.isOn
        newContact.drivingLicenseDescription = drivingLicenseField.text
        newContact.notes = notesField.text
        newContact.photo = contactsPhoto.image
        Contact.contacts.append(newContact)
        print (Contact.contacts)
    }
    
    let datePicker = UIDatePicker()
    let numberPicker = UIPickerView()
    var newContact = Contact()
    var height = [1,0,0]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        birthdayField.inputView = datePicker
        heightField.inputView = numberPicker
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureCancel))
        let birthdayDoneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneBirthdayAction))
        birthdayDoneButton.tintColor = .white
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.isTranslucent = true
        toolbar.barTintColor = UIColor.systemBlue
        toolbar.setItems([flexibleSpace, birthdayDoneButton, flexibleSpace], animated: true)
        toolbar.backgroundColor = UIColor(red: 0, green: 122, blue: 255, alpha: 1)
        birthdayField.inputAccessoryView = toolbar
        
        let heightToolbar = UIToolbar()
        heightToolbar.sizeToFit()
        heightToolbar.isTranslucent = true
        heightToolbar.barTintColor = UIColor.systemBlue
        let heightDoneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneHeightAction))
        heightDoneButton.tintColor = .white
        heightToolbar.backgroundColor = UIColor(red: 0, green: 122, blue: 255, alpha: 1)
        heightToolbar.setItems([flexibleSpace, heightDoneButton, flexibleSpace], animated: true)
        heightField.inputAccessoryView = heightToolbar
        self.view.addGestureRecognizer(tapGesture)
        
        numberPicker.dataSource = self
        numberPicker.delegate = self
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    @objc func doneBirthdayAction() {
        view.endEditing(true)
        getDateFromPicker()
    }
    
    @objc func doneHeightAction() {
        view.endEditing(true)
        getHeightFromPicker()
    }
    
    @objc func tapGestureCancel() {
        view.endEditing(true)
    }
    
    func getDateFromPicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyy"
        birthdayField.text = formatter.string(from: datePicker.date)
    }
    
    func getHeightFromPicker() {
        var textHeight = ""
        for character in height {
            textHeight += "\(character)"
        }
        heightField.text = textHeight
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return 9
        } else {
            return 10
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return "\(row + 1)"
        } else {
            return "\(row)"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            height[component] = row + 1
        } else {
            height[component] = row
        }
    }
}

// MARK: Work with image
extension NewContactTableViewController: UIImagePickerControllerDelegate {
    
    func choseImagePicker(source: UIImagePickerController.SourceType) {
        
        if UIImagePickerController.isSourceTypeAvailable(source) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = source
            present(imagePicker, animated: true)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        contactsPhoto.image = info[.editedImage] as? UIImage
        contactsPhoto.contentMode = .scaleAspectFill
        contactsPhoto.clipsToBounds = true
        contactsPhoto.layer.cornerRadius = contactsPhoto.frame.size.width / 2
        dismiss(animated: true)
    }
}
