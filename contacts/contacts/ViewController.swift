//
//  ViewController.swift
//  contacts
//
//  Created by Руслан Луценко on 18.10.2021.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBAction func emptyAddContact(_ sender: UIButton) {
        
    }
    
    let idCell = "Cell"
    var showArray = [1,2,3,4,5,6,7,8,9,0]
    //var showArray = [Int]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.showArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: idCell)!
        cell.textLabel!.text = String(self.showArray[indexPath.row])
        return cell
    }
}
