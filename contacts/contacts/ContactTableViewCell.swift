//
//  ContactTableViewCell.swift
//  contacts
//
//  Created by Руслан Луценко on 27.10.2021.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
    
    @IBOutlet weak var contactDescription: UILabel!
    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var contactPhoneNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
